import { Button } from '@material-ui/core';
import useStyleButtonError from './styles/ButtonErrorStyle';

function ButtonPage ({error, theme}) {
    const classesButton = useStyleButtonError({error});

    return (
    <Button variant="contained" color="secondary"  className={classesButton.button}>
        Secondary
    </Button>
  );
}

export default ButtonPage;