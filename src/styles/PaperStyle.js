import { makeStyles } from "@material-ui/core";

const useStylePaper = makeStyles((theme) => ({
    paperStyle: {
        width: theme.spacing(50),
        padding: theme.spacing(5),
    } 
}));

export default useStylePaper;