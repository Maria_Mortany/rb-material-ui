import { hexToRgb, makeStyles } from "@material-ui/core";

const useStyleButtonError = makeStyles((theme) => ({
    button: {backgroundColor: (props) => {
        if (props.error) {return hexToRgb(theme.palette.error.main)};
    }}}
));

export default useStyleButtonError;