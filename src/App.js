import './App.css';
import { Button, ThemeProvider, Paper, Grid } from '@material-ui/core';
import useMuiTheme from './theme/useMuiTheme';
import useStylePaper from './styles/PaperStyle';
import ButtonPage from './ButtonPage';

function App() {
  const theme = useMuiTheme();
  const classesPaper = useStylePaper(theme);
  const error = "Error";

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <ButtonPage error={error} theme={theme}/>
        <Grid container
        direction="row"
        justify="center"
        alignItems="center">
          <Paper item elevation={3} className={classesPaper.paperStyle}>
            <Grid container
            spacing={4}
            direction="row"
            justify="space-between"
            alignItems="center"
            > 
              <Grid item xs={12} sm={6}>
                <Button  variant="outlined" color="primary">
                  Primary
                </Button>
              </Grid>
              <Grid item xs={12} sm={6}>
                <ButtonPage error={error} theme={theme}>
                  Secondary
                </ButtonPage>
              </Grid>
              <Grid item xs={12} sm={6} >
                <Button variant="outlined" disabled>
                  Disabled
                </Button>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Button variant="outlined" color="primary" href="#outlined-buttons">
                  Link
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </ThemeProvider>
  );
}

export default App;
