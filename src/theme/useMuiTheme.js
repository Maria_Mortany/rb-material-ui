import { createMuiTheme } from "@material-ui/core";

function useMuiTheme() {
    return createMuiTheme({
        spacing: 4,
        palette: {
        primary: {
            main: "#0F4780",
        },
        secondary: {
            main: "#6B64FF",
        },
        },
        typography: {
            button: {
              fontSize: '16px',
              fontWeight: 'bold',
            },
        },
        overrides: {
            MuiButton: {
                root: {
                    padding: "10px 16px",
                    width: "100%",
                },
                contained: {"&:hover": {opacity: 0,},}
            },
            MuiPaper: {
                root: {borderRadius: "8px",},
            },
        },
    });
}

export default useMuiTheme;